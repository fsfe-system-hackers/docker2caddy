<!--
SPDX-FileCopyrightText: 2021 Free Software Foundation Europe e.V. <https://fsfe.org>

SPDX-License-Identifier: GPL-3.0-only
-->

# Docker2Caddy

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/docker2caddy/00_README)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/docker2caddy)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/docker2caddy)

This tool automatically generates configuration files for reverse proxy
functionality between Caddy and Docker containers. It provides something like
auto-discoverability, so if a new Docker container is spun up, it will be
scanned sooner or later for specific labels. If they match the tool's
expectations, Caddy configuration for this host will be generated.


## Container configuration

To allow caddy to be a reverse proxy for a docker container, and docker2caddy
find the necessary information, you'll have to add labels to the docker
container. In a `docker-compose.yml` file, this could look like the following:

```
webservice:
  container_name: webservice
  image: httpd:2.4
  restart: always
  ports:
    - "12345:8080"
  labels:
    proxy.host: "example.com"
    proxy.port: "12345"
    proxy.host_alias: "example.org,www.example.com"
```

In this example, the exposed port in the Docker container is `8080`, and we map
it to port `12345` on the host.

The labels can be configured in `config.cfg`.

## Install

### docker2caddy

To deploy docker2caddy on a host, these files should be handled.

* One of the caddy config templates in `caddy_host_templates_/` or an own one ->
  `/etc/docker2caddy/caddy_host.j2` (`basic.j2` is the simplest)
* `config.cfg` -> `/etc/docker2caddy/` (rename from `.sample`)
* `docker2caddy.py` -> `/usr/local/bin/docker2caddy`
* `logrotate.d/docker2caddy` -> `/etc/logrotate.d/` (must be root:root, 0644)
* `systemd/docker2caddy.service` -> `/etc/systemd/system/`


### caddy

Within caddy, you need to import the new directory in which the configs are
created:

`import /etc/caddy/docker2caddy/*`

## Troubleshooting

### Docker rootless

If you have a rootless Docker setup, you should set the `DOCKER_HOST` config
variable, e.g. to `unix:///run/user/1001/docker.sock`. Please note that a
system-wide environment (not only from `.bashrc`) or one that is directly called
with the program (e.g. `DOCKER_HOST=foobar docker2caddy.py` or a prior `export`)
always takes precedence.

## Logs and Debug

Logging will take place in `/var/log/docker2caddy.log`. Logs are rotated daily,
and kept back for the last 7 days.

Also check systemd's journal: `journalctl -u docker2caddy`

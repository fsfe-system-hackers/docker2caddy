#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe e.V. <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-only

"""
A script that runs continously and checks all running docker containers. It
looks for labels indicating to be picked up by a Caddy reverse proxy, and
generates configuration for it automatically.
"""

import configparser
import os
import re
import logging
import sys
import subprocess
import docker
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.events import EVENT_JOB_ERROR
from jinja2 import Template


# =============================================================================
# Configs
# =============================================================================

# Read config file
config = configparser.ConfigParser()
config.read("/etc/docker2caddy/config.cfg")
config = config["DEFAULT"]
try:
    CADDYFILE = config.get("CADDYFILE", "/etc/caddy/Caddyfile")
    CHECK_INTERVAL = int(config.get("CHECK_INTERVAL", 30))
    CONFIG_DIR = config.get("CONFIG_DIR", "/etc/caddy/docker2caddy/")
    DEBUG = config.getboolean("DEBUG", "False")
    DOCKER_HOST = config.get("DOCKER_HOST", "")
    LABEL_PROXY_ALIASES = config.get("LABEL_PROXY_ALIASES", "proxy.host_alias")
    LABEL_PROXY_HOST = config.get("LABEL_PROXY_HOST", "proxy.host")
    LABEL_PROXY_PORT = config.get("LABEL_PROXY_PORT", "proxy.port")
    LOGFILE = config.get("LOGFILE", "/var/log/docker2caddy.log")
    OFFLINE_FOR = int(config.get("OFFLINE_FOR", 2880))
    TEMPLATE = config.get("TEMPLATE", "/etc/docker2caddy/caddy_host.j2")
except KeyError:
    logging.error(
        "Config file /etc/docker2caddy/config.cfg not found, or not complete."
    )
    sys.exit(1)


# Configure logging
logging.basicConfig(
    format="[%(asctime)s] (%(name)s) %(levelname)s: %(message)s",
    filename=LOGFILE,
    encoding="utf-8",
    level=(logging.DEBUG if DEBUG else logging.INFO),
)
if not DEBUG:
    logging.getLogger("apscheduler").setLevel(logging.WARNING)


# Set custom DOCKER_HOST env if configured, and if not already in environment
if DOCKER_HOST and "DOCKER_HOST" not in os.environ:
    logging.debug(
        "Using %s as DOCKER_HOST environment variable as configured in config file",
        DOCKER_HOST,
    )
    os.environ["DOCKER_HOST"] = DOCKER_HOST
elif DOCKER_HOST and "DOCKER_HOST" in os.environ:
    # Warning when DOCKER_HOST has been set, but system/direct call has different variable
    logging.warning(
        "NOT using configured %s as DOCKER_HOST environment variable because "
        "env variable with value %s already existent in system",
        DOCKER_HOST,
        os.environ["DOCKER_HOST"],
    )


# =============================================================================
# Global variables
# =============================================================================

# Create empty cache and offline variable, and their pre-copies
_CACHE = []
_OFFLINE = {}


# =============================================================================
# Functions
# =============================================================================

# Functions for Docker and Caddy
def docker_get_hosts():
    """
    Get all running Docker containers and extract necessary info for domain,
    port, aliases, and the container name
    """
    data = []

    # Connect to Docker using the set env
    docker_client = docker.from_env()

    for container in docker_client.containers.list():
        name = container.name
        labels = container.labels

        if all(key in labels for key in (LABEL_PROXY_HOST, LABEL_PROXY_PORT)):
            host = labels[LABEL_PROXY_HOST]
            port = labels[LABEL_PROXY_PORT]

            if LABEL_PROXY_ALIASES in labels:
                aliases = [x.strip() for x in labels[LABEL_PROXY_ALIASES].split(",")]
            else:
                aliases = None

            data.append({"name": name, "host": host, "port": port, "aliases": aliases})

    return data


def caddy_checks():
    """Run various checks about what could go wrong with Caddy's config"""
    # Create config dir for generated caddy config files
    try:
        os.makedirs(CONFIG_DIR, exist_ok=True)
    except PermissionError:
        logging.error(
            "%s could not be created because of missing privileges. Make "
            "sure docker2python runs as root, or change the owner/permissions of "
            "the directory accordingly.",
            CONFIG_DIR,
        )
        sys.exit(1)

    with open(CADDYFILE, encoding="utf8") as caddyfile:
        if not re.search(
            fr"^\s*import\s*{CONFIG_DIR}/?\*\s*$", caddyfile.read(), re.MULTILINE
        ):
            logging.warning(
                "%s does not seem to be imported in %s. "
                "This could mean the generated configuration files by docker2caddy "
                "will not be loaded.",
                CONFIG_DIR,
                CADDYFILE,
            )


def caddy_reload():
    """Reload caddy via caddy reload"""
    result_caddyreload = subprocess.run(
        ["/usr/bin/caddy", "reload", "--config", CADDYFILE],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        check=True,
    )
    result_caddyreload.check_returncode()


def compare_states(on_list, key, compare):
    """Iterate through a list (on_list) of dictionaries, and check whether the
    value of the searched key (key) matches the searched value (compare)"""
    return next(
        (i for i, item in enumerate(on_list) if item[key] == compare),
        None,
    )


def caddy_config_from_docker():
    """Go through running containers, and generate config if not equal to _CACHE"""
    reload = False  # default reload value

    # Get all running containers and their relevant labels
    running_containers = docker_get_hosts()
    for proxied_container in running_containers:
        # Check if the container with unchanged config is in _CACHE
        if proxied_container not in _CACHE:
            # trigger reload because we have a new container
            reload = True

            # Check if at least the host is found in the _CACHE, so config has been changed
            cache_partial = compare_states(_CACHE, "host", proxied_container["host"])

            # If that is the case, delete the old entry from the _CACHE to avoid
            # duplicated hosts with differing config
            if isinstance(cache_partial, int):
                logging.info("%s has been changed", proxied_container["host"])
                del _CACHE[cache_partial]
            else:
                logging.info("%s has been added", proxied_container["host"])

            # Add new container config to _CACHE
            _CACHE.append(proxied_container)

            # Fill Jinja template and save it to caddy's config folder
            with open(TEMPLATE, encoding="utf8") as template:
                hostconfig = Template(template.read()).render(
                    host=proxied_container["host"],
                    port=proxied_container["port"],
                    aliases=proxied_container["aliases"],
                )

                with open(
                    f"{CONFIG_DIR}/{proxied_container['host']}", "w+", encoding="utf8"
                ) as caddyconfig:
                    caddyconfig.truncate()
                    caddyconfig.write(hostconfig)

        else:
            # Make sure the running container is not in _OFFLINE. This solves the
            # issue that a container's offline count increments even if it
            # meanwhile was online again
            try:
                del _OFFLINE[proxied_container["host"]]
                logging.debug(
                    "%s online again after an offline period.",
                    proxied_container["host"],
                )
            except KeyError:
                pass

    return reload, running_containers


def caddy_prepare_directory():
    """Clean caddy config directory. To be called on startup of docker2caddy."""
    for file in os.listdir(CONFIG_DIR):
        os.remove(os.path.join(CONFIG_DIR, file))


def caddy_cleanup(running_containers):
    """See existing caddy config files and check whether all of them are
    currently running. If not, flag for delayed deletion, and delete eventually"""
    reload = False  # default reload value

    # Iterate all caddy files on CONFIG_DIR
    for caddyhost in os.listdir(CONFIG_DIR):
        # Check if the file name (=host name) is found in the currently running containers
        host_in_running_containers = compare_states(
            running_containers, "host", caddyhost
        )

        # If not, the file's host does no longer seem to be online. Increment offline counter
        if not isinstance(host_in_running_containers, int):
            logging.debug("%s is offline.", caddyhost)
            _OFFLINE.setdefault(caddyhost, 0)
            _OFFLINE[caddyhost] += 1

            # If the host has been offline OFFLINE_FOR times in a row, delete the file
            if _OFFLINE[caddyhost] >= OFFLINE_FOR:
                logging.info(
                    "%s repeatedly offline. Removing its caddy config file.", caddyhost
                )

                reload = True  # trigger to reload caddy config

                # Delete this host's config file
                os.remove(os.path.join(CONFIG_DIR, caddyhost))
                # Delete host from _OFFLINE
                del _OFFLINE[caddyhost]
                # Delete host from _CACHE
                try:
                    del _CACHE[compare_states(_CACHE, "host", caddyhost)]
                except:  # pylint: disable=W0702
                    pass

    return reload


def docker2caddy():
    """Function that generates caddy config from running Docker containers, and
    handles cleanups. This is run in the background."""

    # Create caddy config based on currently running containers
    reload_new, running_containers = caddy_config_from_docker()

    # Check for existing caddy config without respective running docker container
    reload_cleanup = caddy_cleanup(running_containers)

    # Check for reload indicators of new or deleted docker containers
    # If any of them triggered, reload caddy
    if reload_new or reload_cleanup:
        logging.info("Running containers changed. Reloading caddy.")
        try:
            caddy_reload()
        except subprocess.CalledProcessError as error:
            logging.error(
                "Reload of caddy threw non-zero exit code %s with following message: %s",
                error.returncode,
                error.stderr.decode("utf-8"),
            )
            raise
    else:
        logging.debug("No changes in running containers. Not reloading caddy.")


# =============================================================================
# APScheduler Background Job Config
# =============================================================================

# Function to shutdown job on error
def sched_shutdown(event):
    """Shutdown scheduler when called"""
    if sched.running and event != "":
        sched.shutdown(wait=False)


sched = BlockingScheduler()
sched.add_job(docker2caddy, IntervalTrigger(seconds=CHECK_INTERVAL))
sched.add_listener(sched_shutdown, EVENT_JOB_ERROR)


# =============================================================================
# Main Function
# =============================================================================


def main():
    """Main function"""
    # One-time check for caddy options
    caddy_checks()
    # Clean the configuration directory
    caddy_prepare_directory()
    # Run caddy configurator once before scheduler starts
    docker2caddy()
    # Start scheduled job
    try:
        sched.start()
    except (KeyboardInterrupt, SystemExit):
        sched.shutdown(wait=False)


# Run the program
main()
